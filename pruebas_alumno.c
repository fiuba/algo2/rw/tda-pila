#include "pila.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>


/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/

void pruebas_pila_alumno() {
    pila_t* pila = NULL;
    print_test("Puntero inicializado a NULL", pila == NULL);
    
    pila = pila_crear();
    print_test("Pila creada", pila != NULL);
    
    print_test("Tamaño de pila = 0", pila_esta_vacia(pila) == true);
    
    pila_destruir(pila);
    print_test("Pila destruida", true);
    
    pila = pila_crear();
    print_test("Pila creada", pila != NULL);
    
    int a = 1;
    
    print_test("Pila apilado: 1", pila_apilar(pila, &a));
    int *n = pila_ver_tope(pila);
    print_test("Pila tope = 1", *n == 1);
    
    
}
